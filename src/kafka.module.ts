import { ConfigService } from '@nestjs/config';
import { ProducerService } from './producer';
import { ConsumerService } from './consumer';

export class KafkaModule {
	static forRoot({ broker }) {
		return {
			module: KafkaModule,
			providers: [
				{
					provide: ProducerService,
					useFactory: (configService: ConfigService) => {
						return new ProducerService(configService, broker);
					}
				},
				{
					provide: ConsumerService,
					useFactory: (configService: ConfigService) => {
						return new ConsumerService(configService, broker);
					}
				}
			],
			exports: [ProducerService, ConsumerService]
		};
	}
}
