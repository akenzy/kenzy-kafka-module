import { Injectable, OnApplicationShutdown } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ConsumerConfig, ConsumerSubscribeTopics, KafkaMessage } from 'kafkajs';
import { IConsumer } from './consumer.interface';
import { KafkajsConsumer } from './kafkajs.consumer';

interface KafkajsConsumerOptions {
	topics: ConsumerSubscribeTopics;
	config: ConsumerConfig;
	onMessage: (message: KafkaMessage) => Promise<void>;
	onError?: (message: KafkaMessage) => Promise<void>;
}

@Injectable()
export class ConsumerService implements OnApplicationShutdown {
	private readonly consumers: IConsumer[] = [];

	constructor(
		private readonly configService: ConfigService,
		private readonly broker: string
	) {}

	async consume({
		topics,
		config,
		onMessage,
		onError
	}: KafkajsConsumerOptions) {
		const consumer = new KafkajsConsumer(topics, config, this.broker);
		await consumer.connect();
		await consumer.consume(onMessage, onError);
		this.consumers.push(consumer);
	}

	async onApplicationShutdown() {
		for (const consumer of this.consumers) {
			await consumer.disconnect();
		}
	}
}
