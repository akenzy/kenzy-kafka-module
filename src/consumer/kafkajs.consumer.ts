import { Logger } from '@nestjs/common';
import {
	Consumer,
	ConsumerConfig,
	ConsumerSubscribeTopics,
	Kafka,
	KafkaMessage
} from 'kafkajs';
import { IConsumer } from './consumer.interface';
import { retryPromise } from '@akenzy/retry';
const sleep = (ms: number) => new Promise(resolve => setTimeout(resolve, ms));

export class KafkajsConsumer implements IConsumer {
	private readonly kafka: Kafka;
	private readonly consumer: Consumer;
	private readonly logger: Logger;

	constructor(
		private readonly topics: ConsumerSubscribeTopics,
		config: ConsumerConfig,
		broker: string | string[]
	) {
		const brokers = Array.isArray(broker) ? broker : [broker];
		this.kafka = new Kafka({ brokers: brokers });
		this.consumer = this.kafka.consumer(config);
		this.logger = new Logger(`${topics.topics.join('_')}-${config.groupId}`);
	}

	async consume(
		onMessage: (message: KafkaMessage) => Promise<void>,
		onError?: (message: KafkaMessage) => Promise<void>
	) {
		await this.consumer.subscribe(this.topics);
		await this.consumer.run({
			eachMessage: async ({ message, partition }) => {
				this.logger.debug(`Processing message partition: ${partition}`);

				try {
					await retryPromise<void>({
						promise: onMessage(message),
						nthTry: 3
					});
				} catch (err) {
					this.logger.error(
						'Error consuming message. Adding to dead letter queue...',
						err
					);
					if (onError) {
						await onError(message);
					}
				}
			}
		});
	}

	async connect() {
		try {
			await this.consumer.connect();
		} catch (err) {
			this.logger.error('Failed to connect to Kafka.', err);
			await sleep(5000);
			await this.connect();
		}
	}

	async disconnect() {
		await this.consumer.disconnect();
	}
}
