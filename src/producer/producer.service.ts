import { Injectable, OnApplicationShutdown } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { IProducer } from './producer.interface';
import { KafkajsProducer } from './kafkajs.producer';

@Injectable()
export class ProducerService implements OnApplicationShutdown {
	private producers = new Map<string, IProducer>();

	constructor(
		private readonly configService: ConfigService,
		private readonly broker: string | Array<string>
	) {}

	private async getProducer(topic: string): Promise<IProducer> {
		let produce = this.producers.get(topic);
		if (produce) {
			return produce;
		}

		produce = new KafkajsProducer(topic, this.broker);
		await produce.connect();
		this.producers.set(topic, produce);

		return produce;
	}

	public async produce(topic: string, message: any): Promise<void> {
		const producer = await this.getProducer(topic);
		await producer.produce(message);
	}

	public async onApplicationShutdown(): Promise<void> {
		for (const producer of this.producers.values()) {
			await producer.disconnect();
		}
	}
}
