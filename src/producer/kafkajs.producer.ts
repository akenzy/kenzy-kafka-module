import { Logger } from '@nestjs/common';
import { Kafka, Message, Producer } from 'kafkajs';
import { IProducer } from './producer.interface';

const sleep = (ms: number) => new Promise(resolve => setTimeout(resolve, ms));

export class KafkajsProducer implements IProducer {
	private readonly kafka: Kafka;
	private readonly producer: Producer;
	private readonly logger: Logger;

	constructor(private readonly topic: string, broker: string | Array<string>) {
		const brokers = Array.isArray(broker) ? broker : [broker];
		this.kafka = new Kafka({
			brokers: brokers
		});
		this.producer = this.kafka.producer();
		this.logger = new Logger(topic);
	}

	async produce(message: Message) {
		await this.producer.send({
			topic: this.topic,
			messages: [message]
		});
	}

	async connect() {
		try {
			await this.producer.connect();
		} catch (err) {
			this.logger.error('Failed to connect to Kafka.', err);
			await sleep(5000);
			await this.connect();
		}
	}

	async disconnect() {
		await this.producer.disconnect();
	}
}
